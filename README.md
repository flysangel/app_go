# app golang

## Git clone

```bash
$ git clone https://gitlab.com/flysangel/app_golang.git
```

## Test app go

1. Go update all modules

```bash
$ go get -u
$ go mod tidy
```

2. Build image

```bash
$ sudo ./run.sh build
Storing signatures
--> cd4dab1e8ed
Successfully tagged quay.io/flysangel/image:app.golang
cd4dab1e8edc206eff8c383f25ce513157cfe556660a250d922d7f8331abda17
```

3. Test app go

```bash
$ sudo podman play kube podman.yaml
Pod:
5cdb7098e953584e22a3a282b8c8764784ede2f26085476651f7e45b824c69af
Container:
a097026b73f87c5a9c6befa4a319017beb0db7b4904e20a8ba554848e5c48b62

$ sudo podman pod ps
POD ID        NAME        STATUS      CREATED             INFRA ID      # OF CONTAINERS
5cdb7098e953  golang      Running     About a minute ago  11b86f64a94a  2

$ sudo podman ps -a
CONTAINER ID  IMAGE                               COMMAND     CREATED        STATUS            PORTS                   NAMES
11b86f64a94a  registry.k8s.io/pause:3.5                            7 seconds ago  Up 7 seconds ago  0.0.0.0:8080->8080/tcp  5cdb7098e953-infra
a097026b73f8  quay.io/flysangel/image:app.golang              7 seconds ago  Up 7 seconds ago  0.0.0.0:8080->8080/tcp  golang-server

$ curl -w '\n' localhost:8080
```

| tools    |               |
| :--------| :------------ |
| ping     | Check health  |
| whois    | Get source ip |
| :echo    | echo          |

| system   |               |
| :--------| :------------ |
| time     | Get time      |

| system   |               |
| :--------| :------------ |
| host     | Get host info |
| hostname | Get hostname  |
| cpu      | cpu info      |
| mem      | mem info      |
| disk     | disk info     |
| net      | net info      |

## License

[Apache License](./LICENSE).
