package main

import (
	router "main/routers"
)

func main() {
	// 初始化
	r := router.InitRouter()
	r.SetTrustedProxies(nil)

	// 綁定 port number
	r.Run(":8080") // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
