#!/bin/bash

#set -x # debug mode
set -a # var export
set -o errexit -o nounset -o pipefail

# function
usage() {
  cat <<EOF
Usage: 
  $(basename "${BASH_SOURCE[0]}") [options]

Available options:

build    build image from run.list
pull     pull image from run.list
push     push image from run.list
EOF
  exit
}

if ! ls /root &>/dev/null; then
  echo 'Please use "sudo bash" run' && exit 1
fi

source run.conf

case "${1-}" in
build)
  for image in ${IMAGES}; do
    echo "${image}" | grep '#' && continue
    buildah rmi quay.io/flysangel/"${REPO}":"${image}" || true
    buildah bud \
      --pull=false \
      --layers=false \
      -t quay.io/flysangel/"${REPO}":"${image}" \
      -f Dockerfile."${image}" .
  done
  ;;

pull)
  echo 'Login quay.io' && buildah login quay.io
  for image in ${IMAGES}; do
    echo "${image}" | grep '#' && continue
    buildah images | grep -q "${image}" || buildah pull quay.io/flysangel/"${REPO}":"${image}"
  done
  ;;

push)
  echo 'Login quay.io' && buildah login quay.io
  for image in ${IMAGES}; do
    echo "${image}" | grep '#' && continue
    buildah images | grep -q "${image}" && buildah push quay.io/flysangel/"${REPO}":"${image}"
  done
  ;;

*)
  usage
  ;;

esac

set +a # var unexport
